package edu.towson.cis.cosc442.project2.rectangle;

// TODO: Auto-generated Javadoc
/**
 * The Class Rectangle.
 */
public class Rectangle {
	
	/** The p2. */
	private Point left, right;
	
	/**
	 * Instantiates a new rectangle.
	 *
	 * @param left the p1
	 * @param right the p2
	 */
	Rectangle(Point left, Point right) {
		this.left = left;
		this.right = right;
	}
	
	/**
	 * Gets the area.
	 *
	 * @return the area
	 */
	public Double getArea() {
		return Math.abs((right.length - left.length) * (right.width - left.width));
	}
	
	/**
	 * Gets the diagonal.
	 *
	 * @return the diagonal
	 */
	public Double getDiagonal() {
		return Math.sqrt(Math.pow((right.length - left.length), 2) + Math.pow((right.width - left.width), 2));
	}
}
