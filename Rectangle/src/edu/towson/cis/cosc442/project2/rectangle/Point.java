package edu.towson.cis.cosc442.project2.rectangle;

/**
 * The Point Class.
 */
public class Point {
	
	/** x and y coordinates. */
	public Double length, width;
	
	/**
	 * Instantiates a new point.
	 *
	 * @param length the x
	 * @param width the y
	 */
	Point(Double length, Double width) {
		this.length = length;
		this.width = width;
	}
}
