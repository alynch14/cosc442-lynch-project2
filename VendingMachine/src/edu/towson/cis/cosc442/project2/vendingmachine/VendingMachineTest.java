package edu.towson.cis.cosc442.project2.vendingmachine;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.hamcrest.CoreMatchers.is;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

// TODO: Auto-generated Javadoc
/**
 * The Class VendingMachineTest.
 */
public class VendingMachineTest {
	
	/** The vending machine for all tests. */
	private VendingMachine myMachine;
	
	/** The item 1 for testing VendingMachine.addItem(). */
	private VendingMachineItem item1;
	
	/** The item 2 for testing VendingMachine.addItem(). */
	private VendingMachineItem item2;
	
	/** The item 3 for testing VendingMachine.removeItem(). */
	private VendingMachineItem item3;
	
	/** The item 4 for testing VendingMachine.purchaseItem(). */
	private VendingMachineItem item4;
	
	/** The Constant CHIPS. */
	private final static String CHIPS = "Chips";
	
	/** The Constant CHIP_PRICE. */
	private final static double CHIP_PRICE = 1.75;
	
	/** The Constant CHIP_CODE. */
	private final static String CHIP_CODE = "A";
	
	/** The Constant PRETZELS. */
	private final static String PRETZELS = "Pretzels";
	
	/** The Constant PRETZEL_PRICE. */
	private final static double PRETZEL_PRICE = 1.25;
	
	/** The Constant PRETZEL_CODE. */
	private final static String PRETZEL_CODE = "B";
	
	/** The Constant CODE_OCCUPIED. */
	private static final String CODE_OCCUPIED = "Slot " + CHIP_CODE + " already occupied";
	
	/** The Constant COOKIES. */
	private static final String COOKIES = "Cookies";
	
	/** The Constant COOKIE_CODE. */
	private static final String COOKIE_CODE = "C";
	
	/** The Constant COOKIE_PRICE. */
	private static final double COOKIE_PRICE = 2.0;
	
	/** The Constant SLOT_EMPTY. */
	private static final String SLOT_EMPTY = "Slot C is empty -- cannot remove item";
	
	/** The Constant DONUTS. */
	private static final String DONUTS = "Donuts";
	
	/** The Constant DONUT_CODE. */
	private static final String DONUT_CODE = "D";
	
	/** The Constant DONUT_PRICE. */
	private static final double DONUT_PRICE = 2.5;
	
	/** The Constant WALLET. */
	private static final double WALLET = 5.0;
	
	/** The Constant NEGATIVE_WALLET. */
	private static final double NEGATIVE_WALLET = -1.0;
	
	/** The Constant INVALID_AMOUNT. */
	private static final String INVALID_AMOUNT = "Invalid amount.  Amount must be >= 0";
	
	/**
	 * Initializes myMachine and all of the items to be used later in the different
	 * test cases.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		myMachine = new VendingMachine();
		item1 = new VendingMachineItem(CHIPS, CHIP_PRICE);
		item2 = new VendingMachineItem(PRETZELS, PRETZEL_PRICE);
		item3 = new VendingMachineItem(COOKIES, COOKIE_PRICE);
		item4 = new VendingMachineItem(DONUTS, DONUT_PRICE);
	}

	/**
	 * Sets all of the pointers to null.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		myMachine = null;
		item1 = null;
		item2 = null;
		item3 = null;
		item4 = null;
	}

	/**
	 * Tests the addItem() method of the VendingMachine class.
	 */
	@Test
	public void addItemTest() {
		myMachine.addItem(item1, CHIP_CODE);
		assertSame("Item was not added correctly.", item1, myMachine.getItem(CHIP_CODE));
		
		myMachine.addItem(item2, PRETZEL_CODE);
		assertSame("Item was not added correctly.", item2, myMachine.getItem(PRETZEL_CODE));
	}
	
	/**
	 * Tests whether the VendingMachineException is thrown for invalid input.
	 */
	@Test
	public void addItemExceptionThrownTest() {
		myMachine.addItem(item1, CHIP_CODE);
		
		try {
			myMachine.addItem(item1, CHIP_CODE);
			fail("Expected a VendingMachineException to be thrown");
		} catch(VendingMachineException myVendingException) {
			assertThat(myVendingException.getMessage(), is(CODE_OCCUPIED));
		}
	}

	/**
	 * Tests whether the removeItem method removes an item correctly.
	 */
	@Test
	public void removeItemTest() {
		myMachine.addItem(item3, COOKIE_CODE);
		myMachine.removeItem(COOKIE_CODE);
		
		assertNull(myMachine.getItem(COOKIE_CODE));
	}
	
	/**
	 * Tests whether a VendingMachineException is thrown for invalid input.
	 */
	@Test
	public void removeItemExceptionThrownTest() {
		try {
			myMachine.removeItem(COOKIE_CODE);
			fail("Exception was not thrown.");
		} catch(VendingMachineException myVendingException) {
			assertThat(myVendingException.getMessage(), is(SLOT_EMPTY));
		}
	}
	
	/**
	 * Tests whether the insertMoney method inserts money into the machine
	 * correctly.
	 */
	@Test
	public void insertMoneyTest() {
		myMachine.insertMoney(WALLET);
		
		assertTrue(myMachine.getBalance()-WALLET < 0.005);
	}
	
	/**
	 * Checks whether a VendingMachineException is thrown when a negative
	 * value is passed into the insertMoney method.
	 */
	@Test
	public void insertNegativeMoneyTest() {
		try {
			myMachine.insertMoney(NEGATIVE_WALLET);
			fail("Negative balance was inserted into wallet");
		} catch (VendingMachineException myVendingException) {
			assertThat(myVendingException.getMessage(), is(INVALID_AMOUNT));
		}
	}
	
	/**
	 * Tests whether the makePurchase method returns true when the item
	 * exists in the vending machine and the user has a high enough balance
	 * in their wallet.
	 */
	@Test
	public void makePurchaseTest() {
		myMachine.addItem(item4, DONUT_CODE);
		myMachine.insertMoney(WALLET);
		
		assertTrue(myMachine.makePurchase(DONUT_CODE));
	}
	
	/**
	 * Failed purchase no money test.
	 */
	@Test
	public void failedPurchaseNoMoneyTest() {
		myMachine.addItem(item4, DONUT_CODE);
		myMachine.insertMoney(1);
		
		assertFalse(myMachine.makePurchase(DONUT_CODE));
	}
	
	/**
	 * Failed purchase no item test.
	 */
	@Test
	public void failedPurchaseNoItemTest() {
		myMachine.insertMoney(WALLET);
		
		assertFalse(myMachine.makePurchase(DONUT_CODE));
	}
	
	/**
	 * Return change test.
	 */
	@Test
	public void returnChangeTest() {
		myMachine.insertMoney(WALLET);
		
		assertTrue(myMachine.returnChange()-WALLET < 0.005);
	}
}
