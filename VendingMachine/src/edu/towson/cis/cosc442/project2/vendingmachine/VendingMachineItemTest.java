package edu.towson.cis.cosc442.project2.vendingmachine;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

// TODO: Auto-generated Javadoc
/**
 * The Class VendingMachineItemTest.
 */
public class VendingMachineItemTest {
	
	/** The item 1. */
	private VendingMachineItem item1;
	
	/** The Constant WATER. */
	private static final String WATER = "Water";
	
	/** The Constant PRICE. */
	private static final double PRICE = 1.25;
	
	/** The Constant NEGATIVE_PRICE. */
	private static final double NEGATIVE_PRICE = -1.0;
	
	/** The test constructor. */
	private VendingMachineItem testConstructor;
	
	/** The Constant PRICE_LESS_THAN_ZERO_MESSAGE. */
	private final static String PRICE_LESS_THAN_ZERO_MESSAGE = "Price cannot be less than zero";
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		item1 = new VendingMachineItem(WATER, PRICE);
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		item1 = null;
	}

	/**
	 * Constructor test.
	 */
	@Test
	public void constructorTest() {
		try {
			testConstructor = new VendingMachineItem(WATER, NEGATIVE_PRICE);
			fail("Object was initialized with a negative price");
		} catch (VendingMachineException myVendingException) {
			assertThat(myVendingException.getMessage(), is(PRICE_LESS_THAN_ZERO_MESSAGE));
		}
	}
	
	/**
	 * Gets the name test.
	 *
	 * @return the name test
	 */
	@Test
	public void getNameTest() {
		assertEquals(WATER, item1.getName());
	}
	
	/**
	 * Gets the price test.
	 *
	 * @return the price test
	 */
	@Test
	public void getPriceTest() {
		assertTrue(item1.getPrice()-PRICE < 0.005);
	}

}
